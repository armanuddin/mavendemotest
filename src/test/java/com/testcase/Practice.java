package com.testcase;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.test.utilities.BaseTest;
import com.test.webelement.WebElements;

import Config.PropertyFile;
import Config.ReadExcel;
public class Practice extends BaseTest{

	public static WebDriver driver;

	@BeforeMethod
	public void launchBrowser() throws IOException {
		driver= launchApplication();
	}
	@Test
	public void test() throws InterruptedException, IOException{  
		WebElements locateElements=PageFactory.initElements(driver, WebElements.class);
		//locateElements.MyFunction();
		ReadExcel re=new ReadExcel();
		re.excel_sheet("/home/arman/Desktop/ExcelData/TestData.xlsx");
		locateElements.loginMethod(re.getData(0, 1, 0), re.getData(0, 1, 1));
		//locateElements.loginMethod(PropertyFile.getPropertyFile("username"), PropertyFile.getPropertyFile("password"));
		Thread.sleep(2000);
		locateElements.clicking();
		Thread.sleep(8000);
		//		driver.findElement(By.xpath("//div[@class='nM']/div/div/div[@role='button']")).click();
		locateElements.compose_mail(PropertyFile.getPropertyFile("Sentto"), PropertyFile.getPropertyFile("messa"));
	}
	@AfterMethod
	public static void closeBrowser() throws InterruptedException{
		WebElements locateElements1=PageFactory.initElements(driver, WebElements.class);
		locateElements1.logoutMethod();
		driver.quit();
	}


}
