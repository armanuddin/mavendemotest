package Config;
import java.util.*;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

public class ReadFillo {
	//static Fillo f;

	/*public void readfile() throws Exception{   

		Connection connection=f.getConnection("/home/arman/Desktop/ExcelData/TestData.xlsx");
		String strQuery="Select * from prac";
		Recordset recordset=connection.executeQuery(strQuery);
		recordset.close();
		connection.close();
		while(recordset.next())
		{
			System.out.println(recordset.getField("Details"));
		}
	}*/

	public static List<List<String>>  executeQuery(String fileName,
			String query) throws FilloException{

		return executeQuery(fileName, query);
	}

	public static String executeUpdateQuery(String fileName, String query)
			throws FilloException {

		Fillo fillo = new Fillo();
		Connection connection = fillo.getConnection(fileName);
		connection.executeUpdate(query);
		connection.close();

		return "Update was Successful";
	}


}
