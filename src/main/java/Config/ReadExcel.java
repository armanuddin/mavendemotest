package Config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	static FileInputStream fis;
	static File f;
	static XSSFWorkbook wb;
	static XSSFSheet sh;
	static FileOutputStream fo;

	public void excel_sheet(String ExcelPath ) throws IOException
	{   
		f=new File(ExcelPath);
		fis=new FileInputStream(f);
		wb=new XSSFWorkbook(fis);
	}
	public String getData(int SheetNum,int Row,int Col) {
		sh=wb.getSheetAt(SheetNum);
		String Data=sh.getRow(Row).getCell(Col).getStringCellValue();
		return Data;
	}
	public int getRowCount(int sheetIndex) {
		int row=wb.getSheetAt(sheetIndex).getLastRowNum();
		row=row+1;
		return row;
	}
}