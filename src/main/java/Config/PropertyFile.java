package Config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyFile{
	static File f;
	static FileInputStream fi;
	static Properties p;
	static String PropertyPath=("/home/arman/git/mavendemotest/testData.properties");

	public static String getPropertyFile(String key) throws IOException{   
		f=new File(PropertyPath);
		fi=new FileInputStream(f);
		p=new Properties();
		p.load(fi);
		String value = p.getProperty(key);
		return value;
	}
}