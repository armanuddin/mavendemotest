package com.test.webelement;


import static org.testng.Assert.assertTrue;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebElements {
	WebDriver driver;
	WebDriverWait wait; 
	@FindBy(name="identifier") WebElement username;
	@FindBy(xpath="//*[@id='password']//input[@name='password']") WebElement password;
	@FindBy(how=How.ID,using="identifierNext") WebElement next;
	@FindBy(how=How.ID,using="passwordNext") WebElement next1;
	@FindBy(xpath="//*[@id=\"gb\"]//span[@class=\"gb_xa gbii\"]") WebElement logout;
	@FindBy(xpath="//div[@aria-label='Account Information']//a[contains(text(),'Sign out')]") WebElement logout1;
	@FindBy(xpath="//*[@id='lang-chooser']") WebElement learnmore;
	@FindBy(xpath="//div[@class='nM']/div/div/div") WebElement Compose;
	@FindBy(xpath="//*[@id=':pk']") WebElement SendTo;
	@FindBy(xpath="//*[@id=':q7']") WebElement Message;
	@FindBy(xpath="//*[@id=':os']") WebElement Send;
	@FindBy(xpath="//*[@id='gbwa']") WebElement dot;
	@FindBy(xpath="//*[@id='gb23']") WebElement ggg;
	//@FindBy(xpath="//*[@id=':k5']") WebElement Sent;
	//@FindBy(partialLinkText="sarmanuddinahmad") WebElement SentMessage;

	public WebElements(WebDriver driver1){
		this.driver=driver1;
		wait = new WebDriverWait(driver,30);
	}
	public void clicking(){
		dot.click();
		ggg.click();
	}
	public void loginMethod(String uid,String upwd) throws InterruptedException{
		username.sendKeys(uid);
		next.click();
		Thread.sleep(2000);
		password.sendKeys(upwd);
		Thread.sleep(2000);
		next1.click();
	}	

	public void compose_mail(String sendTo,String messg) throws InterruptedException{  
		Thread.sleep(8000);
		Compose.click();
		SendTo.sendKeys(sendTo);
		Message.sendKeys(messg);
		Send.click();
	}

	//	public void delet_mail()
	//	{
	//		Sent.click();
	//	}

	/*public void MyFunction(){
		wait.until(ExpectedConditions.visibilityOf(password));
	}*/

	public void logoutMethod() throws InterruptedException{   
		Thread.sleep(2000);
		logout.click();
		Thread.sleep(2000);
		logout1.click();
		Thread.sleep(2000);
		assertTrue(learnmore.isDisplayed());
		System.out.println("LogOut Successfully");
	}
	public void selectDropDown(WebElement DropDown_ele, int value) {
		Select sel=new Select(DropDown_ele);
		wait.until(ExpectedConditions.visibilityOf(DropDown_ele));
		try {
			if(DropDown_ele.isDisplayed()) {
				sel.selectByIndex(value);
			}
		}catch(Exception e) {
			System.out.println("FAILED");
		}
	}
	public void selectCheckbox(WebElement CheckBox_ele) {
		wait.until(ExpectedConditions.visibilityOf(CheckBox_ele));
		try {
			if(CheckBox_ele.isSelected()) {
				CheckBox_ele.clear();
				CheckBox_ele.click();
			}
		}catch(Exception e) {
			System.out.println("not selected");
		}
	}
	public void selectRadioButton(WebElement RadioBtn_ele,String status) {
		wait.until(ExpectedConditions.visibilityOf(RadioBtn_ele));

	}
}
