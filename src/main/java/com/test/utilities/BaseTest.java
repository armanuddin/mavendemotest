package com.test.utilities;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import Config.PropertyFile;
import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseTest{

	public WebDriver webDriver = null;
	public WebDriver launchApplication() throws IOException{
		if(PropertyFile.getPropertyFile("browser").equalsIgnoreCase("chrome")){
			WebDriverManager.chromedriver().setup();
			webDriver = new ChromeDriver();
			webDriver.manage().window().maximize();
			webDriver.get((PropertyFile.getPropertyFile("Url")));	
		}else if(PropertyFile.getPropertyFile("browser").equalsIgnoreCase("firefox")){
			WebDriverManager.firefoxdriver().setup();
			webDriver = new FirefoxDriver();
			webDriver.manage().window().maximize();
			webDriver.get(PropertyFile.getPropertyFile("Url"));	
		}
		return webDriver;
	}
}
